package com.jenkins;

import static org.testng.Assert.assertEquals;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SeleniumwithJenkinsTest {
	WebDriver driver;
	@BeforeClass
	public void beforeClass() throws MalformedURLException {
		driver = new ChromeDriver();
		//driver.get("https://testgloballogic.learnupon.com/users/sign_in");
		driver.get("https://www.demoblaze.com/index.html");
		driver.manage().window().maximize();
		//driver.manage().timeouts().implicitlyWait(30,timeunits.of)
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	@AfterClass
	public void afterClass() {
		driver.quit();
	}
	@Test(priority=1)

	public void login() throws MalformedURLException, InterruptedException
	{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.partialLinkText("Log in")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("loginusername")).sendKeys("manzoormehadi");
		driver.findElement(By.id("loginpassword")).sendKeys("Mehek@110");
		driver.findElement(By.xpath("//button[text()='Log in']")).click();
		Thread.sleep(2000);
		WebElement nameuser = driver.findElement(By.id("nameofuser"));
		Thread.sleep(2000);
		//WebElement nameuser = driver.findElement(By.partialLinkText("Welcome manzoormehadi"));
		String name = nameuser.getText();
		System.out.println(name);
		assertEquals(name, "Welcome manzoormehadi");
}
	@Test(priority=2)
	public void addProduct() throws InterruptedException
	{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.findElement(By.linkText("Nexus 6")).click();
	driver.findElement(By.linkText("Add to cart")).click();
	Thread.sleep(2000);
	Alert alert=driver.switchTo().alert();
	String popup = alert.getText();
	System.out.println(popup);
	assertEquals(popup, "Product added.");
	Thread.sleep(1000);
	driver.switchTo().alert().accept();
	
	
	}
	@Test(priority=3)
	public void payment() throws InterruptedException
	{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.findElement(By.linkText("Cart")).click();
	driver.findElement(By.xpath("//button[text()='Place Order']")).click();
	driver.findElement(By.id("name")).sendKeys("Nargees");
	driver.findElement(By.id("country")).sendKeys("india");
	driver.findElement(By.id("city")).sendKeys("chennai");
	driver.findElement(By.id("card")).sendKeys("6543210987654321");
	driver.findElement(By.id("month")).sendKeys("August");
	driver.findElement(By.id("year")).sendKeys("2025");
	Thread.sleep(1000);
	driver.findElement(By.xpath("//button[text()='Purchase']")).click();
	WebElement message = driver.findElement(By.xpath("//h2[contains(text(),'Thank you')]"));
	String purchasemesage = message.getText();
	assertEquals(purchasemesage, "Thank you for your purchase!");
	Thread.sleep(2000);
	driver.findElement(By.xpath("//button[text()='OK']")).click();
	}
	@Test(priority=4)
	public void logout() throws InterruptedException
	{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(2000);
		driver.findElement(By.linkText("Log out")).click();
		String logout=driver.findElement(By.xpath("//a[@id='login2']")).getText();
		Thread.sleep(2000);
		assertEquals(logout, "Log in");
	}
	
}
